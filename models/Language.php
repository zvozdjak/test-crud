<?php

namespace app\models;

use yii\db\ActiveRecord;
use yii\db\Expression;
use Yii;

class Language extends ActiveRecord
{
	public static function tableName()
    {
        return 'language';
    }
}