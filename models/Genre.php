<?php

namespace app\models;

use yii\db\ActiveRecord;
use yii\db\Expression;
use Yii;

class Genre extends ActiveRecord
{
	public static function tableName()
    {
        return 'genre';
    }
}