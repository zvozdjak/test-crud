<?php

namespace app\models;

use yii\db\ActiveRecord;
use yii\db\Expression;
use Yii;
use yii\web\UploadedFile;
use app\models\Author;
use app\models\Genre;
use app\models\Language;

class Books extends ActiveRecord
{
	public $query_param;

	public static function tableName()
    {
        return 'books';
    }

    public function scenarios()
    {
        return [
            'default' => ['id', 'title', 'author', 'genre', 'language', 'publication_date', 'isbn', ' image', 'query_param'],
        ];
    }


    public function rules()
    {
        return [
        	//['isbn', 'isbnValidation', 'skipOnError' => false],
            [['title', 'publication_date','query_param'], 'safe'],
            [['title', 'publication_date', 'isbn'], 'required'],
            [['image'], 'file', 'extensions' => 'png, jpg'],
            ['isbn', 'match', 'pattern' => '/^ISBN:(\d{12}(?:\d|X))$/']
        ];
    }

    public function upload()
    {
        if ($this->validate()) {
            $this->image->saveAs('images/' . $this->image->baseName . '.' . $this->image->extension);
            return true;
        } else {
            return false;
        }
    }

    public function isbnValidation($attribute_name, $params)
    {
    	$regex = '/\b(?:ISBN(?:: ?| ))?((?:97[89])?\d{9}[\dx])\b/i';

	    if (preg_match($regex, str_replace('-', '', $this->isbn), $matches)) {
	        if(10 === strlen($matches[1])){
	        	$this->addError($attribute_name, 'Valid ISBN 10 but, not valid ISBN 13 format');
    			return false;
    		}else{
    			return true;
    		}
	    }else{   		    
    		$this->addError($attribute_name, 'Not valid ISBN format');
    		return false;
	    }

    }

    public function getCurrentAuthor()
    {
        if(isset($this->author)){
            return $this->hasOne(Author::className(), ['id' => 'author']);
        }else{
            return new Author();
        } 
    }

    public function getAllAuthor()
    {
        $modelContent = Author::find()->all();
        return $modelContent;
    }

    public function getCurrentGenre()
    {
        if(isset($this->genre)){
            return $this->hasOne(Genre::className(), ['id' => 'genre']);
        }else{
            return new Genre();
        } 
    }

    public function getAllGenre()
    {
        $modelContent = Genre::find()->all();
        return $modelContent;
    }

    public function getCurrentLanguage()
    {
        if(isset($this->language)){
            return $this->hasOne(Language::className(), ['id' => 'language']);
        }else{
            return new Language();
        } 
    }

    public function getAllLanguage()
    {
        $modelContent = Language::find()->all();
        return $modelContent;
    }


}