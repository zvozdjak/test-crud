<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Books */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="books-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>


    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'author')->dropDownList(
                            ArrayHelper::map($model->allAuthor, 'id', 'author') ,
                            [
                                'prompt' => 'Select Author',
                            ]); ?>
    
    <?= $form->field($model, 'genre')->dropDownList(
                            ArrayHelper::map($model->allGenre, 'id', 'genre') ,
                            [
                                'prompt' => 'Select Genre',
                            ]); ?>

    <?= $form->field($model, 'language')->dropDownList(
                            ArrayHelper::map($model->allLanguage, 'id', 'language') ,
                            [
                                'prompt' => 'Select Language',
                            ]); ?>

    <?= $form->field($model, 'publication_date')->textInput(['maxlength' => true, 'type'=> 'number', 'min' => '0']) ?>

    <?= $form->field($model, 'isbn')->textInput(['maxlength' => true])->label('ISBN (isbn-13)') ?>
    <?php if($model->image): ?>
    <img src="/web/images/<?=$model->image?>" alt="" height="100px;">
    <?php endif; ?>
    <?= $form->field($model, 'image')->fileInput(['required' => $model->image ? false : true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
