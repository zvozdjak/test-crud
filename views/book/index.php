<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\BooksSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Books';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="books-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php echo $this->render('_search', ['model' => $searchModel]); ?>
    <p>
        <?= Html::a('Create Books', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'tableOptions' => [
            'class' => 'table table-striped table-bordered table-hover'
        ],
        'columns' => [

            [
                'attribute' => 'title',
                'format' => 'raw',
                'value' => function ($model) {    
                    return Html::decode('
                        <span>'.$model->title.'</span>
                        <a href="/web/book/update/*?id='.$model->id.'" title="Update" aria-label="Update" class="hidden" data-pjax="0">
                            <span class="glyphicon glyphicon-pencil"></span>
                        </a>
                        <img src="/web/images/'.$model->image.'" class="imgLink">');

                },
            ],
            [
                'contentOptions' =>['class' => 'table_class','style'=>'display:block;'],
                'header' => 'Author',
                'attribute' => 'author',
                'value' => function ($model) {    
                    return $model->currentAuthor->author;
                },
            ],
            [
                'header' => 'Genre',
                'attribute' => 'genre',
                'value' => function ($model) {    
                    return $model->currentGenre->genre;
                },
            ],
            [
                'header' => 'Language',
                'attribute' => 'language',
                'value' => function ($model) {    
                    return $model->currentLanguage->language;
                },
            ],
            [
                'header' => 'Date',
                'attribute' => 'publication_date',
            ],
            [
                'header' => 'ISBN',
                'attribute' => 'isbn',
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'visibleButtons' => [
                    'update' => false
                ]
            ],
        ],
    ]); ?>
</div>


