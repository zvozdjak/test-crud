<?php

use yii\db\Schema;
use yii\db\Migration;

class m170220_113565_author extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%author}}', [
            'id' => Schema::TYPE_PK,
            'author' => Schema::TYPE_STRING . '(255) NOT NULL',
        ], $tableOptions);
    }

    public function safeDown()
    {
        $this->dropTable('{{%author}}');
    }
}
