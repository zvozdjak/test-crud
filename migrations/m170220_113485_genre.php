<?php

use yii\db\Schema;
use yii\db\Migration;

class m170220_113485_genre extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%genre}}', [
            'id' => Schema::TYPE_PK,
            'genre' => Schema::TYPE_STRING . '(255) NOT NULL',
        ], $tableOptions);
    }

    public function safeDown()
    {
        $this->dropTable('{{%genre}}');
    }
}
