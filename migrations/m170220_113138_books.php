<?php

use yii\db\Schema;
use yii\db\Migration;

class m170220_113138_books extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%books}}', [
            'id' => Schema::TYPE_PK,
            'title' => Schema::TYPE_STRING . '(255) NOT NULL',
            'author'=> Schema::TYPE_INTEGER,
            'genre'=> Schema::TYPE_INTEGER,
            'language'=> Schema::TYPE_INTEGER,
            'publication_date' => Schema::TYPE_STRING . '(255) NOT NULL',
            'isbn' => Schema::TYPE_STRING . '(255) NOT NULL',
            'image' => Schema::TYPE_STRING . '(255) NOT NULL',
        ], $tableOptions);
    }

    public function safeDown()
    {
        $this->dropTable('{{%books}}');
    }
}
